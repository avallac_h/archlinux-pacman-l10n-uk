#!/bin/sh
set -e

BASEDIR=$(dirname "$0")

install -vDm0644                                                    \
    "${BASEDIR}/for_translation_archlinux-pacman_libalpm-pot_uk.mo" \
    "/usr/share/locale/uk/LC_MESSAGES/libalpm.mo"

install -vDm0644                                                    \
    "${BASEDIR}/for_translation_archlinux-pacman_pacman-pot_uk.mo"  \
    "/usr/share/locale/uk/LC_MESSAGES/pacman.mo"

install -vDm0644                                                            \
    "${BASEDIR}/for_translation_archlinux-pacman_pacman-scripts-pot_uk.mo"  \
    "/usr/share/locale/uk/LC_MESSAGES/pacman-scripts.mo"

echo "All done." >&2

